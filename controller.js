angular
  .controller('advancedInAppBrowser', loadFunction);

loadFunction.$inject = ['$scope', 'structureService', '$location', '$filter'];

function loadFunction($scope, structureService, $location, $filter) {
  //Register upper level modules
  structureService.registerModule($location, $scope, 'advancedInAppBrowser');

  //launch spinner
  structureService.launchSpinner('.transitionloader');
  $scope.isBusy = true;

  //get user options
  var config = $scope.advancedInAppBrowser.modulescope;
  $scope.showIframe = false;

  $scope.url = config.url

  if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
    document.addEventListener("deviceready", onDeviceReady, false);
  } else {
    loadIframe();
    $scope.isBusy = false;
  }


  function onDeviceReady() {
    var ref = cordova.InAppBrowser.open(config.url, '_blank', 'zoom=no,location=no,toolbar=no,enableViewportScale=yes');
    //ref.hide();

    //create resources to inject
    var insertJs = composeJs();
    var insertCss = composeCss();

    ref.addEventListener('loadstart', function (event) {
         console.log(event.url);
      if(event.url.indexOf("koaSaveMe") > -1){
         ref.close();
         backToApp();
      }else if(event.url.indexOf("#/") != -1){
        ref.close();
        backToApp(event.url);
      }

      //show loader while code insert
      $scope.isBusy = true;
      //ref.hide();
    });

    ref.addEventListener('loadstop', function () {
      //insert css and js in the inappBrowser
         if(insertCss != ""){
           ref.insertCSS({code: insertCss});
         }
         if(insertJs != "(function() {})()" ){
           ref.executeScript({ code: insertJs});
         }

      //show inappBrowser
      //ref.show();
    });


    ref.addEventListener('exit', function (event) {
        $scope.isBusy = false;
    });

  }

  function composeCss(){
    var finalStyle = "";

    if(config.hideHead && !config.customHead){
      var headid = config.headerId != "" && config.headerId.indexOf("#") < 0? `#${config.headerId}`: config.headerId;
      var headCss = `header${headid}{display:none;}`;
      finalStyle += headCss;
    }

    if(config.hideFooter && !config.customFooter){
      var footerId = config.footerId != "" && config.footerId.indexOf("#") < 0? `#${config.footerId}`: config.footerId;
      var footerCss = `footer${footerId}{display:none;}`;
      finalStyle += footerCss;
    }

    return finalStyle;
  }

  function composeJs(){
    var finalJs = "(function() {";

    if(config.customHead){
      if(config.customHeadCode === ""){
        //defoult header
        var headid = config.headerId != "" && config.headerId.indexOf("#") < 0? `#${config.headerId}`: config.headerId;

        var deps = "<script src='https://kit.fontawesome.com/a076d05399.js'></script><link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>";
        var styles = "<style>#koaheader{display: flex;justify-content: space-between;flex-direction: row;width: 100%;background: #3f51b5;color: white;height: 56px;align-items: center; padding-left: 11px; box-shadow: 0px 1px 8px 1px black;}</style>";

       // var scripts = "<script>function back(){ if(history.length <= 0){window.close();}else{window.history.back();} }</script>";
        var html = "<div id='koaheader'><i class='fa fa-arrow-left' id='arrowBack' style='font-size:24px; height:100%; display: flex; align-items: center;'></i><h1>Menu name</h1> <div class='space'></div></div> ";

        //search header
        finalJs += `document.querySelector(\" header${headid} \" ).innerHTML = \" `;
        finalJs += deps;
        finalJs += styles;
        finalJs += html + " \";";
        finalJs += "document.getElementById('arrowBack').addEventListener('click', function(){";
        finalJs += "console.log(location.href,window.location.origin);";
        finalJs += "if(location.href === window.location.origin + '/'){";
        finalJs += "window.location.assign('koaSaveMe');";
        finalJs += "}else{";
        finalJs += "window.history.back();";
        finalJs += "}});";
      }else{
        //custom header
        finalJs += `document.querySelector(\" header${headid} \" ).innerHTML =`;
        finalJs += config.customHeadCode + ";";
      }
    }
    if(config.customFooter){
        var footerId = config.footerId != "" && config.footerId.indexOf("#") < 0? `#${config.footerId}`: config.footerId;
        //custom footer
        finalJs += `document.querySelector(\" footer${footerId} \" ).innerHTML = \" `;
        finalJs += config.customFooterCode + ";";
    }
    if(config.showMenu){
      finalJs += "var koaMenu = document.createElement('div');";
      finalJs += "koaMenu.id='koaMenu' ;";
      finalJs += "koaMenu.innerHTML = \" ";
      if(config.menucss === "" && config.menuScript === ""){
          //defoult menu
          finalJs += "<style>";
          finalJs += "#koaMenu{";
          finalJs += "  position: sticky;";
          finalJs += "  bottom:0;";
          finalJs += "  width: 100%;";
          finalJs += "  height: 50px;";
          finalJs += "  z-index: 999;";
          finalJs += "  display:flex;";
          finalJs += "  flex-direction:row;";
          finalJs += "  justify-content:space-between;";
          finalJs += "}";
          finalJs += "#koaMenu a{";
          finalJs += "  width: 100%;";
          finalJs += "  height: 100%;";
          finalJs += "  display: flex;";
          finalJs += "  justify-content: center;";
          finalJs += "  align-items: center;";
          finalJs += "  background: white;";
          finalJs += "  border: 1px solid;";
          finalJs += "  font-size: 34px;";
          finalJs += "}";
          finalJs += "</style>";
          finalJs += getMenuLinks();
      }else{
          finalJs += "<style>";
          finalJs += config.menucss;
          finalJs += "</style>";
          finalJs += getMenuLinks();
      }
      finalJs += " \"; ";
      finalJs += config.menuScript? config.menuScript: "";
      finalJs += "if(!document.getElementById('koaMenu')){ document.body.appendChild(koaMenu);}";
      finalJs += "var menu = document.getElementById('koaMenu');";
      finalJs += "menu.addEventListener('click', function(event){";
      finalJs += "  var url = event.target.href ? event.target.href: event.target.parentElement.href ? event.target.parentElement.href:false;";
      finalJs += "  if(url.indexOf('#/') != -1)location.href='http://' + url ;";
      finalJs += "});";
    }

    finalJs += "})()";
    return finalJs;
  }

  function loadIframe() {
    $scope.showIframe = true;
    setTimeout(function () {
      structureService.launchSpinner('.transitionloader');
    }, 100);
  }

   function backToApp(path) {
     var finalPath = structureService.get().config.index;
     if(config.toBack && !path){
        finalPath = config.toBack;
     }else if(path){
        finalPath = "/" + path.split("#/")[1];
     }

      console.log(finalPath);
     $location.path(finalPath);
     if(!$scope.$$phase) {
       $scope.$apply();
     }
   }

    function getMenuLinks() {
      var promicess = [];
      var links = "";

      config.menuItems.map(function(item){
        var url = item.path ? `#${item.path}`: item.internalUrl ? item.internalUrl : "";
        var icon = item.icon ? item.icon : "";
        var text = item.text ? item.text : "";

           links += `<a href=${url}> ${icon} <span>${text}</span></a>`;
      });

      return links;
    }
}
